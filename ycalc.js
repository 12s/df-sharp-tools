// ==UserScript==
// @name         Yahoo DF calc
// @namespace    https://sports.yahoo.com/dailyfantasy/*
// @version      0.1
// @description  calc percentages
// @author       --
// @include      https://sports.yahoo.com/dailyfantasy/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    console.clear();

    // Step 1: Add Banner UI

    // Step 1.1: Banner
    let bannerNode = document.createElement("div"); // top level
    // Banner style
    bannerNode.style.width = "100%";
    bannerNode.style.minHeight = "40px";
    bannerNode.style.background = "black";
    bannerNode.style.zIndex = "1001";
    bannerNode.style.fontFamily = "Courier New";
    bannerNode.style.display = "flex";
    // Banner into DOM
    const bodyNode = document.getElementsByTagName("body")[0];
    const documentNode = bodyNode.parentElement;
    documentNode.insertBefore(bannerNode, bodyNode);

    // Step 1.2: Board
    let boardNode = document.createElement("div");
    boardNode.classList.add("boardNode");
    // Board style
    boardNode.style.flex = "1";
    boardNode.style.color = "white";
    boardNode.style.fontSize = "15px";
    boardNode.style.padding = "3px";

    // Board into DOM
    bannerNode.appendChild(boardNode);

    // Step 1.3: Config
    let configNode = document.createElement("div");
    // Config style
    configNode.style.color = "white";
    configNode.style.display = "flex";
    configNode.style.flexDirection = "column";
    configNode.style.color = "white";
    // Config into Dom
    bannerNode.appendChild(configNode);

    // Step 1.3.1 Threshold Wrapper, Text and Input
    let thresholdWrapper = document.createElement("div");
    thresholdWrapper.style.display = "flex";
    thresholdWrapper.style.color = "white";
    thresholdWrapper.style.alignItems = "center";
    let thresholdText = document.createElement("span");
    thresholdText.innerText = "Threshold % : ";
    let thresholdInput = document.createElement("input");
    thresholdInput.classList.add("thresholdInput");
    thresholdInput.style.width = "33px";
    thresholdInput.style.color = "black";
    thresholdInput.style.textAlign = "right";
    thresholdInput.style.fontFamily = "Courier New";
    thresholdInput.style.marginLeft = "6px";
    thresholdInput.style.height = "21px";
    thresholdInput.type = "text";
    thresholdInput.value = "100";
    thresholdInput.style.width = "40px";

    // Lineup
    let lineupWrapper = document.createElement("div");
    let lineupText = document.createElement("span");
    lineupText.innerText = "Additional Entries";
    let additionalEntries = document.createElement("input");
    additionalEntries.classList.add("additionalEntries");
    additionalEntries.style.width = "33px";
    additionalEntries.style.color = "black";
    additionalEntries.style.textAlign = "right";
    additionalEntries.style.fontFamily = "Courier New";
    additionalEntries.style.marginLeft = "6px";
    additionalEntries.style.height = "21px";
    additionalEntries.type = "text";
    additionalEntries.value = "3";
    additionalEntries.style.width = "40px";

    let savedLineupWrapper = document.createElement("div");
    savedLineupWrapper.style.display = "flex";
    savedLineupWrapper.style.color = "white";
    savedLineupWrapper.style.alignItems = "center";
    let savedLineupText = document.createElement("span");
    savedLineupText.innerText = "Saved Linup Index Value : ";
    let savedLineupInput = document.createElement("input");
    savedLineupInput.classList.add("savedLineupInput");
    savedLineupInput.style.width = "33px";
    savedLineupInput.style.color = "black";
    savedLineupInput.style.textAlign = "right";
    savedLineupInput.style.fontFamily = "Courier New";
    savedLineupInput.style.marginLeft = "6px";
    savedLineupInput.style.height = "21px";
    savedLineupInput.type = "text";
    savedLineupInput.value = "1";
    savedLineupInput.style.width = "40px";


    // Step 1.3.1 Threshold Wrapper, Text and Input into DOM
    thresholdWrapper.appendChild(thresholdText);
    thresholdWrapper.appendChild(thresholdInput);
    lineupWrapper.appendChild(lineupText);
    lineupWrapper.appendChild(additionalEntries);
    savedLineupWrapper.appendChild(savedLineupText);
    savedLineupWrapper.appendChild(savedLineupInput);

    configNode.appendChild(thresholdWrapper);
    configNode.appendChild(lineupWrapper);
    configNode.appendChild(savedLineupWrapper);



    // Step 1.4: Enter Contest
    let contestNode = document.createElement("div");
    // Contest style
    contestNode.style.width = "200px";
    contestNode.style.textAlign = "right";
    contestNode.style.paddingRight = "5px";
    // Contest into DOM
    bannerNode.appendChild(contestNode);

    // Step 0.4.1 Submit Button
    let submitButton = document.createElement("input");
    submitButton.classList.add("submitButton");
    // Submit style
    submitButton.value = "Refresh Contests";
    submitButton.type  = "submit";
    submitButton.style.background = "lightblue";
    submitButton.style.fontFamily = "Courier New";
    submitButton.style.fontWeight = "bold";
    // Submit Logic
    submitButton.addEventListener("click", function() {
        console.clear();
        console.log(`Populate Contests`);
        let boardNode = document.getElementsByClassName("boardNode")[0];
        while (boardNode.hasChildNodes()) { // clear contests
            boardNode.removeChild(boardNode.lastChild);
        }

        let contestRows = document.querySelectorAll("tr[data-tst='contest-row']");
        let threshold = +document.getElementsByClassName("thresholdInput")[0].value;
        let nodeArray = [];

        for (let i = 0; i < contestRows.length; i++) {
            if (contestRows[i].children[6].children[0].disabled) {
                continue;
            }

            let c = contestRows[i].children[2].textContent;
            let nu = c.substr(0, c.indexOf("/"));
            let entry = contestRows[i].children[3].textContent;
            entry = Number(entry.substr(1, entry.length));
            let prizePool = contestRows[i].children[4].textContent;
            prizePool = Number(prizePool.substr(1, prizePool.length).replace(",",""));

            const d = prizePool / entry;

            if (nu.includes("K")) {
                nu = Number(nu.substr(0, nu.length - 1) * 1000);
            }

            const additionalEntries = Number(document.querySelector(".additionalEntries").value);
            const percent = Math.round((+nu + additionalEntries ) / +d * 100);
            if (isNaN(percent) || threshold < percent) {
                continue;
            }

            const contestNameNode = document.createElement("span");
            contestNameNode.style.height = "10px";

            let n = contestRows[i].children[3].textContent;

            while(n.length < 6) {
                n = n.concat(".");
            }

            const name = contestRows[i].children[1].textContent;
            n = n.concat(" " + contestRows[i].children[2].textContent);

            while(n.length < 17) {
                n = n.concat(".");
            }

            n = n.concat("= " + percent.toString() + "%");

            if (contestRows[i].children[1].textContent.indexOf("[")+1) {
                n = n.concat(" - " + contestRows[i].children[1].textContent.substr(0, contestRows[i].children[1].textContent.indexOf("[")-1)); //strip text
            } else {
                n = n.concat(" - " + contestRows[i].children[1].textContent); //strip text
            }

            const checkbox = document.createElement("input");
            if (name.substr(name.length - 1) === "M") {
                n = n.concat(" - MULTI");
                checkbox.isMulti = true;
            }

            contestNameNode.innerHTML = n;
            contestNameNode.setAttribute("percent", percent);


            checkbox.style.marginRight = "3px";
            checkbox.classList.add("contestCheckbox");
            checkbox.type = "checkbox";
            checkbox.contestId = contestRows[i].getAttribute("data-tst-contest-id");

            const rowNode = document.createElement("div");
            rowNode.style.display = "flex";
            rowNode.style.alignItems = "center";
            rowNode.style.height = "12px";
            rowNode.append(checkbox);
            rowNode.append(contestNameNode);

            nodeArray.push(rowNode);
            // let boardNode = document.getElementsByClassName("boardNode")[0];
            // boardNode.appendChild(rowNode);
        }

        // sort nodeArray
        function sortNumber(a,b) {
            return a.lastChild.getAttribute("percent") - b.lastChild.getAttribute("percent");
        }
        nodeArray.sort(sortNumber);

        for (let k = 0; k < contestRows.length; k++) {
            if (nodeArray[k]){
                boardNode.appendChild(nodeArray[k]);
            }
        }
    });
    // Submit into DOM
    contestNode.appendChild(submitButton);

    // Step 0.4.1 Enter Contest Button
    let enterButton = document.createElement("input");
    enterButton.classList.add("enterButton");
    // Submit style
    enterButton.value = "Enter Contests!";
    enterButton.type  = "submit";
    enterButton.style.background = "lightpink";
    enterButton.style.fontFamily = "Courier New";
    enterButton.style.fontWeight = "bold";
    // Submit Logic
    var target = document.querySelector('html');

    // create an observer instance
    var observer = new MutationObserver(function(mutations) {
        mutations.forEach(function(mutation) {
            console.log("new mutation found");

            let lineupNode = mutation.target.lastChild.firstChild.firstChild.children[3]
            .firstChild.firstChild.children[1];
            if (lineupNode) {
                console.log(`trying to drill to last node..`);
                lineupNode = lineupNode.lastChild.firstChild.lastChild.firstChild.firstChild.lastChild;
            }

            let lineupIndex = Number(document.querySelector(".savedLineupInput").value) - 1;
            if (lineupNode && window.evilHackFlag === 1) {
                lineupNode.children[lineupIndex].lastChild.lastChild.click();
                document.querySelector(".PageOverlay").click();
            }
        });
    });

    // configuration of the observer:
    var config = { attributes: true, childList: true, characterData: true }

    // pass in the target node, as well as the observer options
    observer.observe(target, config);

    enterButton.addEventListener("dblclick", function() {
        // SUPER EVIL GLOBAL HACK SO MUTATION OBSERVER

        let checkboxes = Array.prototype.slice.call(document.querySelectorAll("input.contestCheckbox"));
        checkboxes = checkboxes.filter(checkbox => checkbox.checked);
        window.evilHackFlag = 1;
        function next(checkbox) {
            document.querySelector(`tr[data-tst-contest-id='${checkbox.contestId}']`).click();
        }

        for(let i =0; i<checkboxes.length; i++) {
            (function(i) {
                setTimeout(function() {
                    next(checkboxes[i]);
                    if (i === checkboxes.length-1) {
                      setTimeout(function() {
                        delete window.evilHackFlag;
                      }, 2000);
                    }
                },(i+1) * 1000);
            })(i)
        };
    });

    contestNode.appendChild(enterButton);
})();
